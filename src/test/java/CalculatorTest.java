import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void sumTest(){
        Calculator calculator = new Calculator();
        int actual = calculator.sum(2, 2);

        Assert.assertEquals(4, actual);
    }

    @Test
    public void substractTest(){
        Calculator calculator = new Calculator();
        double actual = calculator.subtract(1.0, -1.3);

        Assert.assertEquals(2.3, actual, 0.1);
    }

    @Test
    public void multiTest(){
        Calculator calculator = new Calculator();
        long actual = calculator.multiply(10, -13);

        Assert.assertEquals(-130, actual);
    }

    @Test
    public void devTest(){
        Calculator calculator = new Calculator();
        double actual = calculator.divide(1, -1);

        Assert.assertEquals(-1, actual, 0.1);
    }

    @Test
    public void tesSum1(){
        Calculator calculator = new Calculator(1, 2);
        int actual = calculator.sum();

        Assert.assertEquals(3, actual);
    }

    @Test
    public void substractTest1(){
        Calculator calculator = new Calculator(1, 2);
        double actual = calculator.subtract();

        Assert.assertEquals(-1, actual, 0.1);
    }

    @Test
    public void multiTest1(){
        Calculator calculator = new Calculator(1, 2);
        int actual = calculator.multiply();

        Assert.assertEquals(2, actual);
    }

    @Test
    public void devTest1(){
        Calculator calculator = new Calculator(1, -1);
        double actual = calculator.divide();

        Assert.assertEquals(-1, actual, 0.1);
    }
}