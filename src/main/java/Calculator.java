public class Calculator {
    private int _a;
    private int _b;
    public Calculator() {
    }
    public Calculator(int a, int b) {
        this._a = a;
        this._b = b;
    }

    public int sum(){
        return _a + _b;
    }

    public static int sum(int a, int b){
        return a + b;
    }

    public double subtract() {
        return _a - _b;
    }
    public static double subtract(double a, double b) {
        return a - b;
    }

    public int multiply(){
        return _a * _b;
    }

    public static long multiply(long a, long b){
        return a * b;
    }

    public double divide(){
        return _a / _b;
    }

    public static double divide(int a, int b){
        return a / b;
    }
}
